#--------------------------------------------------------------
# General
#--------------------------------------------------------------
terraform {
  required_version = ">= 0.12.0"

  backend "s3" {
    bucket         = "testgen-tfstates"
    key            = "global/s3/terraform.tfstate"
    region         = "us-west-2"
    dynamodb_table = "tflocks"
  }
}

provider "aws" {
  region           = "${var.region}"
  access_key       = "${var.accesskey}"
  secret_key       = "${var.secretkey}"
}

#----------------------------------------------------------------
# Data sources to get VPC, subnet, security group and AMI details 
#----------------------------------------------------------------
data "aws_vpc" "default" {
  tags = {
    Name = "${var.vpc_name}"
  }
}

data "template_cloudinit_config" "userdata-cloudinit-config" {
  part {
    content_type = "text/cloud-config"
    content      = "${data.template_file.userdata-template.rendered}"
  }
}

data "template_file" "userdata-template" {
  template = "ssh_authorized_keys:\n  - \"$${TF_USER_PUBKEY}\""

  vars = {
    TF_USER_PUBKEY = "${file("templates/ssh_keys.pub")}"
  }
}
#--------------------------------------------------------------
# VPC
#--------------------------------------------------------------
module "vpc" {
  source = "../modules/vpc"

  vpc_cidr                 = "${var.vpc_cidr}"
  azs                      = "${var.azs}"
  dhcp_options_domain_name = "${var.domain_name}"
  public_subnets           = "${var.public_subnets}"
  private_subnets          = "${var.private_subnets}"
  tags                     = "${var.tags}"
}

#--------------------------------------------------------------
# ElasticIP
#--------------------------------------------------------------
resource "aws_eip" "eip" {
  count = "${var.ec2_count}"

  vpc  = true
  tags = "${merge(var.tags, map("Name", format("%s-0%d", var.ec2_role, count.index + 1)))}"

}

resource "aws_eip_association" "eip_assoc" {
  count         = "${var.ec2_count}"
  instance_id   = "${element(module.ec2.ec2_ids, count.index)}"
  allocation_id = "${element(aws_eip.eip.*.id, count.index)}"
}
#--------------------------------------------------------------
# EC2
#--------------------------------------------------------------
module "ec2" {
  source = "../modules/ec2"

  vpc_name      = "${var.vpc_name}"
  subnet_prefix = "${var.subnet_prefix}"
  ami_name      = "${var.ami_name}"
  ami_owner     = "${var.ami_owner}"

  ec2_count              = "${var.ec2_count}"
  ec2_type               = "${var.ec2_type}"
  user_data              = "${data.template_cloudinit_config.userdata-cloudinit-config.rendered}"
  ec2_role               = "${var.ec2_role}"
  tags                   = "${var.tags}"
}
