variable "region" {}
variable "accesskey" {}
variable "secretkey" {}
variable "aws_region" {}
variable "vpc_cidr" {}
variable "domain_name" {}
variable "azs" {
  type = "list"
}
variable "public_subnets" {
  type = "list"
}
variable "private_subnets" {
  type = "list"
}
variable "tags" {
  type = "map"
}
variable "vpc_name" {}
variable "subnet_prefix" {}
variable "ami_name" {}
variable "ami_owner" {}
variable "ec2_count" {}
variable "ec2_type" {}
variable "ec2_role" {}
variable "ssh_access_enabled" {
  description = "If True, openvpn servers is available by SSH from 0.0.0.0/0"
}
