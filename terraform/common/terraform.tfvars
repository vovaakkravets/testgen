#------------- AWS ---------------
region        = "us-west-2"
accesskey     = "***"
secretkey     = "***"

#------------- VPC ---------------

aws_region      = "us-west-2"
azs             = ["us-west-2a", "us-west-2b", "us-west-2c"]
vpc_cidr        = "10.0.0.0/16"
public_subnets  = ["10.0.0.0/24", "10.0.1.0/24", "10.0.2.0/24"]
private_subnets = ["10.0.10.0/24", "10.0.11.0/24", "10.0.12.0/24"]
domain_name     = "testgen.k.com"

#------------- AMI ----------------
ami_name  = "amzn2-ami-hvm-2.0.20190115-x86_64-gp2"
ami_owner = "137112412989" #amazon

#------------- EC2 -----------------
ec2_count             = 2
ec2_type              = "t2.micro"
ec2_source_dest_check = false
ec2_role              = "lemp"

vpc_name      = "main"
subnet_prefix = "main-public"

ssh_access_enabled    = true

#------------ TAGS -------------
tags = {
  "LaunchedBy"  = "terraform"
  "RP"          = "vovakkavets@gmail.com"
  "Environment" = "main"
  "Application" = "vpc"
}