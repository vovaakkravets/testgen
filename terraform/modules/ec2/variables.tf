variable "vpc_name" {
  description = "VPC name to find VPC ID"
}

variable "subnet_prefix" {
  description = "Subnet prefix to find Subnet IDs"
}

variable "ami_name" {
  description = "Human AMI name to detect AMI ID"
}

variable "ami_owner" {
  description = "AMI owner to detect AMI ID"
}


variable "ec2_count" {
  description = "Count of the instances"
  default     = 1
}

variable "ebs_optimized" {
  description = "If true, the launched EC2 instance will be EBS-optimized"
  default     = false
}

variable "ec2_type" {
  description = "Type of the EC2"
  default     = "t3.micro"
}

variable "ec2_private_ip" {
  description = "Private IP address to associate with the instance in a VPC"
  default     = ""
}

variable "ec2_source_dest_check" {
  description = "Controls if traffic is routed to the instance when the destination address does not match the instance"
  default     = true
}

variable "user_data" {
  description = "The user data to provide when launching the instance"
  default     = ""
}

variable "iam_instance_profile" {
  description = "Name of the Instance Profile"
  default     = ""
}

variable "ec2_role" {
  description = "Role EC2 is the name of the instance"
}

variable "root_volume_type" {
  description = "The type of volume"
  default     = "gp2"
}

variable "root_volume_size" {
  description = "The size of the volume in gibibytes (GiB)"
  default     = 20
}

variable "root_volume_iops" {
  description = "The amount of provisioned IOPS"
  default     = 0
}

variable "ssh_access_enabled" {
  description = "If True, openvpn servers is available by SSH from 0.0.0.0/0"
  default     = true
}

variable "tags" {
  type = "map"
}
