#----------------------------------------------------------------
# Data collection
#----------------------------------------------------------------
data "aws_vpc" "default" {
  tags = {
    Name = "${var.vpc_name}"
  }
}

data "aws_subnet_ids" "subnets" {
  vpc_id = "${data.aws_vpc.default.id}"

  tags = {
    Name = "*${var.subnet_prefix}*"
  }
}

data "aws_ami" "ami" {
  most_recent = true

  filter {
    name   = "name"
    values = ["${var.ami_name}"]
  }

  owners = ["${var.ami_owner}"]
}

#----------------------------------------------------------------
# Create EC2 instance
#----------------------------------------------------------------
resource "aws_instance" "ec2" {
  count = "${var.ec2_count}"

  ami                    = "${data.aws_ami.ami.id}"
  ebs_optimized          = "${var.ebs_optimized}"
  instance_type          = "${var.ec2_type}"
  subnet_id              = "${element(tolist(data.aws_subnet_ids.subnets.ids), count.index)}"
  private_ip             = "${var.ec2_private_ip}"
  user_data              = "${var.user_data}"

  root_block_device {
    volume_type = "${var.root_volume_type}"
    volume_size = "${var.root_volume_size}"
    iops        = "${var.root_volume_iops}"
  }

  tags        = "${merge(var.tags, map("Role", var.ec2_role), map("Name", format("%s-0%d", var.ec2_role, count.index + 1)))}"
}



