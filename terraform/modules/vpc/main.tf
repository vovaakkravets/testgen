#-------------------- VPC -----------------------------
resource "aws_vpc" "this" {
  cidr_block           = "${var.vpc_cidr}"
  enable_dns_support   = "${var.enable_dns_support}"
  enable_dns_hostnames = "${var.enable_dns_hostnames}"

  tags = "${merge(var.tags, map("Name", "main"))}"
}

#-------------------- Defaults ---------------
resource "aws_default_security_group" "default" {
  vpc_id = "${aws_vpc.this.id}"

  ingress {
    protocol  = -1
    self      = true
    from_port = 0
    to_port   = 0
  }

  egress {
    from_port   = 0
    to_port     = 0
    protocol    = "-1"
    cidr_blocks = ["0.0.0.0/0"]
  }

  tags = "${merge(var.tags, map("Name", "main-sg"))}"
}

resource "aws_default_network_acl" "default" {
  default_network_acl_id = "${aws_vpc.this.default_network_acl_id}"

  ingress {
    protocol   = -1
    rule_no    = 100
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 0
    to_port    = 0
  }

  egress {
    protocol   = -1
    rule_no    = 100
    action     = "allow"
    cidr_block = "0.0.0.0/0"
    from_port  = 0
    to_port    = 0
  }

  tags = "${merge(var.tags, map("Name", "main-acl"))}"

  lifecycle {
    ignore_changes = ["subnet_ids"]
  }
}

resource "aws_default_route_table" "default" {
  default_route_table_id = "${aws_vpc.this.default_route_table_id}"

  tags = "${merge(var.tags, map("Name", "main-default"))}"
}

resource "aws_default_vpc_dhcp_options" "default" {
  tags = "${merge(var.tags, map("Name", "default"))}"

  depends_on = ["aws_vpc.this"]
}

#-------------------- DHCP -----------------------------
resource "aws_vpc_dhcp_options" "this" {
  domain_name         = "${var.dhcp_options_domain_name}"
  domain_name_servers = "${var.dhcp_options_domain_name_servers}"

  tags = "${merge(var.tags, map("Name", "main-dhcp"))}"
}

resource "aws_vpc_dhcp_options_association" "this" {
  vpc_id          = "${aws_vpc.this.id}"
  dhcp_options_id = "${aws_vpc_dhcp_options.this.id}"
}

#-------------------- Public subnet -------------------
resource "aws_subnet" "public" {
  count = "${length(var.public_subnets)}"

  vpc_id                  = "${aws_vpc.this.id}"
  cidr_block              = "${element(var.public_subnets, count.index)}"
  availability_zone       = "${element(var.azs, count.index)}"
  map_public_ip_on_launch = true

  tags = "${
    merge(var.tags,
      map("Name", format("main-public-%s", substr(element(var.azs, count.index), -2, -1))),
      )
  }"
}

#-------------------- Private subnet -------------------
resource "aws_subnet" "private" {
  count = "${length(var.private_subnets)}"

  vpc_id                  = "${aws_vpc.this.id}"
  cidr_block              = "${var.private_subnets[count.index]}"
  availability_zone       = "${element(var.azs, count.index)}"
  map_public_ip_on_launch = false

  tags = "${
    merge(var.tags,
      map("Name", format("main-private-%s", substr(element(var.azs, count.index), -2, -1))),
     )
  }"
}

#-------------------- Internet Gateway -------------------
resource "aws_internet_gateway" "this" {
  vpc_id = "${aws_vpc.this.id}"

  tags = "${merge(var.tags, map("Name", "main-ig"))}"
}

#-------------------- NAT Gateway -------------------
resource "aws_eip" "nat" {
  vpc = true

  tags = "${merge(var.tags, map("Name", "main-nat-ip"))}"
}

resource "aws_nat_gateway" "this" {
  allocation_id = "${aws_eip.nat.id}"
  subnet_id     = "${aws_subnet.public.0.id}"

  tags = "${merge(var.tags, map("Name", "main-nat"))}"
}

#-------------------- Route tables -------------------
resource "aws_route_table" "public" {
  vpc_id = "${aws_vpc.this.id}"

  tags = "${merge(var.tags, map("Name", "main-public"))}"
}

resource "aws_route" "public_route" {
  route_table_id         = "${aws_route_table.public.id}"
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = "${aws_internet_gateway.this.id}"
}

resource "aws_route_table" "private" {
  vpc_id = "${aws_vpc.this.id}"

  tags = "${merge(var.tags, map("Name", "main-private"))}"
}

resource "aws_route" "private_route" {
  route_table_id         = "${aws_route_table.private.id}"
  destination_cidr_block = "0.0.0.0/0"
  nat_gateway_id         = "${aws_nat_gateway.this.id}"
}

resource "aws_route_table_association" "public" {
  count = "${length(var.public_subnets)}"

  subnet_id      = "${element(aws_subnet.public.*.id, count.index)}"
  route_table_id = "${aws_route_table.public.id}"
}

resource "aws_route_table_association" "private" {
  count = "${length(var.private_subnets)}"

  subnet_id      = "${element(aws_subnet.private.*.id, count.index)}"
  route_table_id = "${aws_route_table.private.id}"
}
